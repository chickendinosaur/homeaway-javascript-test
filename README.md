Homeaway JavaScript module test.

JavaScript
Problem to Solve
--------------------------------------------------
1.      write a function that takes in an array of strings and appends the date/time to the beginning of each string and returns them back as an array.  (date/time format doesn't matter)

NodeJs
Problem to Solve
------------------------------------------------------
1.      Write the above solution as a very simple, Node module.  Assume the code will be put in to a file named myNodeModule.js.
2.      Write a simple use of the module.

---  

# Specs  

## Performance  

Test value: 
```javascript
let values = ['this', 'is', 'fun'];
```

### myNodeModule
myNodeModule(values) x 651,923 ops/sec  

---  

# Getting Started  

## Installation


## Usage

```javascript
const myNodeModule = require('./lib/myNodeModule');

let values = ['this', 'is', 'fun'];

console.log(myNodeModule(values));

// Result

/*
['2016-11-15T18:40:13.883Zthis',
'2016-11-15T18:40:13.883Zis',
'2016-11-15T18:40:13.883Zfun']
*/
```
---  

# Development  

## Installation  

~/project/:

* npm install
* npm run test

## Build  

* npm run build

## Benchmarking  

* npm run benchmark

## Test  

* npm run test

---  

# License  

The MIT License (MIT)

Copyright (c) 2016 John Pittman

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
