'use strict';

module.exports = function (values) {
	var i = 0;
	var n = values.length;
	var utcDate = new Date().toISOString();

	while (i < n) {
		values[i] = utcDate + values[i];
		++i;
	}

	return values;
};
