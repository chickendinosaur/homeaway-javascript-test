'use strict';

const test = require('tape');
const myNodeModule = require('../../lib/myNodeModule');
const mockValues = require('../../mocks/values');

test('myNodeModule', function (assert) {
	assert.equal(myNodeModule(mockValues)[0].substring(24), 'this', 'Prepends date.');

	assert.end();
});
